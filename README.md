# 卡方检验

[![Netlify Status](https://api.netlify.com/api/v1/badges/83145f3b-a330-473d-9d46-2cf10269ac1c/deploy-status)](https://app.netlify.com/sites/chi2test-api/deploys)

Fortran 的卡方检验包，目前支持皮尔逊卡方检验、Welch T 检验、单样本 T 检验。

**欢迎建议与代码贡献！**

```fortran
use chi2test_m, only: chi2test, t_test, wp
```

## 使用 [fortran-lang/fpm](https://github.com/fortran-lang/fpm) 构建

依赖库：“GSL” 。

```fpm
fpm build --example --all
```

通过 fpm，可以直接引用 chi2test 包：

```toml
[dependencies]
chi2test = { git = "https://gitee.com/zoziha/chi2test" }
```

## API 文档

```sh
ford FORD-project-file.md
```

最新的 API 文档，请访问 [chi2test-api](https://chi2test-api.netlify.app/)。

## 参考链接

- [rosettacode/Fortran 卡方检验](https://rosettacode.org/wiki/Verify_distribution_uniformity/Chi-squared_test#Fortran)
- [rosettacode/Fortran T 检验](https://rosettacode.org/wiki/Welch%27s_t-test#Fortran)
