program main

    use chi2test_m, only: chi2test, wp
    real(wp) :: p, chi
    integer :: df

    real(wp) :: x(2, 3) = reshape([15.0, 85.0, 95.0, 5.0, 5.0, 6.0], [2, 3])
    real(wp) :: y(5) = [199809., 200665., 199607., 200270., 199649.]

    write (*, '(a)') 'Dateset 1:'
    write (*, '(3(f5.1,:,1x))') x(1, :)
    write (*, '(3(f5.1,:,1x))') x(2, :)

    call chi2test(x, p, chi, df)
    write (*, '(a,g10.2)') 'p-value: ', p
    write (*, '(a,g10.2)') 'chi-squared: ', chi
    write (*, '(a,i2)') 'degrees of freedom: ', df

    write (*, '(/a)') 'Dateset 2:'
    write (*, '(*(es9.2,:,1x))') y(:)
    call chi2test(y, p, chi, df)
    write (*, '(a,g10.2)') 'p-value: ', p
    write (*, '(a,g10.2)') 'chi-squared: ', chi
    write (*, '(a,i2)') 'degrees of freedom: ', df

end program main

! Dateset 1:
!  15.0  95.0   5.0
!  85.0   5.0   6.0
! p-value:   0.78E-28
! chi-squared:   0.13E+03
! degrees of freedom:  2
! 
! Dateset 2:
!  2.00E+05  2.01E+05  2.00E+05  2.00E+05  2.00E+05
! p-value:   0.39
! chi-squared:    4.1
! degrees of freedom:  4