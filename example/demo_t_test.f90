program main

    use chi2test_m, only: t_test, wp
    implicit none
    real(wp) :: x(4) = [3.0_wp, 4.0_wp, 1.0_wp, 2.1_wp]
    real(wp) :: y(3) = [490.2_wp, 340.0_wp, 433.9_wp]
    real(wp) :: t, p, df
    integer :: dof

    call t_test(x, y, p, t, df)

    write (*, '(a)') "Welch's T-test:"
    write (*, '(a,g0.4)') "p  = ", p
    write (*, '(a,g0.4)') "t  = ", t
    write (*, '(a,g0.4)') "df = ", df

    call t_test([1.0_wp, 2.0_wp, 3.0_wp], 0.5_wp, p, t, dof)
    write (*, '(/a)') "Student's One Sample T-test:"
    write (*, '(a,g0.4)') "p  = ", p
    write (*, '(a,g0.4)') "t  = ", t
    write (*, '(a,g0.4)') "df = ", dof

    call t_test([1.0_wp, 2.0_wp, 3.0_wp], 3.5_wp, p, t, dof)
    write (*, '(/a)') "Student's One Sample T-test:"
    write (*, '(a,g0.4)') "p  = ", p
    write (*, '(a,g0.4)') "t  = ", t
    write (*, '(a,g0.4)') "df = ", dof

end program main

! Welch's T-test:
! p  = 0.1075E-1
! t  = -9.559
! df = 2.001
!
! Student's One Sample T-test:
! p  = 0.2999E-1
! t  = 3.897
! df = 2