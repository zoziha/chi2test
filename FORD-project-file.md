---
project: zoziha/chi2test
summary: 卡方检验
project_website: https://gitee.com/zoziha/chi2test
project_download: https://gitee.com/zoziha/chi2test/releases
output_dir: API-html
page_dir: doc
md_extensions: markdown.extensions.toc
media_dir: doc/media
author: 左志华
author_description: 哈尔滨工程大学-船舶与海洋结构物设计制造，在读学生
email: zuo.zhihua@qq.com
author_pic: ./media/zoziha.png
website: https://gitee.com/zoziha
preprocess: false
display: public
source: true
graph: false
parallel: 4
print_creation_date: true
creation_date: %Y-%m-%d %H:%M %z
---

{!README.md!}
