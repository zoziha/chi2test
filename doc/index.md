---
title: 规范与帮助
---

# 卡方检验包

[TOC]

## Welch T 检验

在统计学上，Welch t 检验是一种双样本位置检验，用于检验两个总体均值相等的假设。 Welch t 检验是对学生 t 检验的一种改编，当两个样本的方差和样本大小不相等时，其可靠性更高。 这些检验通常称为“未配对”或“独立样本”t 检验。 当比较的两个样本的统计单位不重叠时，通常会应用这些检验。 Welch t 检验不如学生 t 检验常用，读者可能不太熟悉。 该检验也称为“Welch 不等方差 t 检验”或“不等方差 t 检验”。

-- 摘录自[《维基百科/Welch's t-test》](https://en.wikipedia.org/wiki/Welch%27s_t-test)

## 单样本 T 检验

样本标准差：\[S = \sqrt{\frac{\sum_{i=1}^n{(x_i - \mu)}^2}{n-1}}\]
标准误差: \[se = \frac{S}{\sqrt{n}}\]
t 值：\[t = \frac{样本均值 - 总体均值}{标准误差}\]

单样本 t 检验是一种统计学上的假设检验，用于确定未知的总体均值是否与特定的值有差异。

使用 Octave 进行单样本 T 检验：

```matlab
pkg load statistics
x = [1:3];
[h, p, c, s] = ttest(x, 0.5)
[h, p, c, s] = ttest(x, 3.5)
```
